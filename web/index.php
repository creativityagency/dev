<?php

// 1 => Affiche les erreurs PHP , 0 => Cache les erreurs PHP
ini_set('display_errors', 1);

/*
 * Mode maintenance
 * true => active le mode maintenance , false => desactive le mode maintenance
 */

$maintenance = false;

if ($maintenance == true){

    header('Location: ../templates/maintenance/maintenance.html');
} else {

    require_once __DIR__.'/../vendor/autoload.php';

	// Configurations du site + infos serveurs
	require __DIR__.'/../config/prod.php';

	// Parametre Application Silex et Monolog
	$app = require_once __DIR__.'/../src/application/application.php';

	// Controllers du site
	require __DIR__.'/../src/controllers/homepage.php';
	require __DIR__.'/../src/controllers/inscription.php';
	require __DIR__.'/../src/controllers/connexion.php';

	// Lancement de l'application
	$app->run();
}


