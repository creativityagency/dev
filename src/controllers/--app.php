<?php
// Class Database - Gere les methodes BDD
require_once __DIR__ . '/../model/database.php';
// Class FactoryServices - Gere les Custom Fonctions
require_once __DIR__ . '/../services/FactoryServices.php';

// Librairie Silex - Fonctionnement de l'App.
use Silex\Application;
use Silex\Provider\AssetServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\HttpFragmentServiceProvider; 

// use Symfony
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

$app = new Application();
$app->register(new ServiceControllerServiceProvider());
$app->register(new AssetServiceProvider());
$app->register(new TwigServiceProvider());
$app->register(new HttpFragmentServiceProvider());

/*
 * /!\ Dans Silex 2.0 => l'architecture fait en sorte que : 
 * Une fonction qui gère une route est appelée un contrôleur. 
 */

$app->get('/', function () use ($app){

    $pdo = Database::getDatabase();
    $a = $pdo->testtest();
    var_dump($a);

    $pdo = Database::getDatabase();
    $bb = $pdo->recupLog();
    var_dump($bb);

    // Appelle du logger grace a la Factory
    $services = new FactoryServices();
    $testservices = $services->testservices();
    var_dump($testservices);

    // Genere la vue
    ob_start();// start buffering HTML output
    require '../templates/index.html';
    $view = ob_get_clean(); // assign HTML output to $view
    return $view;
})->bind('homepage');
// return $app->redirect($app['url_generator']->generate('homepage'));
// bind permet de nommer un controleur en intrne pour les redirection 

$app->get('/connexion', function () use ($app){
    //require '../src/model.php';
    //$articles = getArticles();
    //var_dump($request) ;
    //var_dump($app);
    ob_start();             // start buffering HTML output
    // require fonctionne pour les include PHP mais pas pour twig.$_COOKIE
    // pour twig faire render ...
    return $app->redirect($app['url_generator']->generate('inscription'));
})->bind('connexion');

$app->get('/inscription', function () use ($app){
    //require '../src/model.php';
    //$articles = getArticles();
    //var_dump($request) ;
    //var_dump($app);
    ob_start();             // start buffering HTML output
    // require fonctionne pour les include PHP mais pas pour twig.$_COOKIE
    // pour twig faire render ...
    return "inscription";
})->bind('inscription');

/* original code
$app->error(function (\Exception $e, Request $request, $code) use ($app) {
    if ($app['debug']) {
        return ;
    }

    // 404.html, or 40x.html, or 4xx.html, or error.html
    $templates = array(
        'errors/'.$code.'.html.twig',
        'errors/'.substr($code, 0, 2).'x.html.twig',
        'errors/'.substr($code, 0, 1).'xx.html.twig',
        'errors/default.html.twig',
    );

    return new Response($app['twig']->resolveTemplate($templates)->render(array('code' => $code)), $code);
});
*/
// Register error handler
$app->error(function (\Exception $e, Request $request, $code) use($app) {
    switch ($code) {
        case 403:
            $message = 'Access denied.';
            break;
        case 404:
            $message = 'The requested resource could not be found.';
            break;
        case 500:
            $message = 'Internal Server Error.';
            break;
        default:
            $message = "Something went wrong.";
    }
    echo '<p>' . $message  . '</p>';
});

return $app