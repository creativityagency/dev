<?php
// Class Database - Gere les methodes BDD
require_once __DIR__ . '/../model/database.php';
// Class FactoryServices - Gere les Custom Fonctions
require_once __DIR__ . '/../services/FactoryServices.php';

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ParameterBag;
use SoundCloudApp\Providers\UserProvider;

$app->get('/', function () use ($app){
    
      /*  $pdo = Database::getDatabase();
        $a = $pdo->testtest();
        var_dump($a);
    
        $pdo = Database::getDatabase();
        $bb = $pdo->recupLog();
        var_dump($bb);
    
        // Appelle du logger grace a la Factory
        $services = new FactoryServices();
        $testservices = $services->testservices();
        var_dump($testservices);
    */
        // Genere la vue $
        ob_start();// start buffering HTML output
        require '../templates/findly/header/header.php';
        require '../templates/findly/footer/footer.php';
        $view = ob_get_clean(); // assign HTML output to $view
        return $view;
    })->bind('homepage');
    // return $app->redirect($app['url_generator']->generate('homepage'));
    // bind permet de nommer un controleur en intrne pour les redirection 

?>