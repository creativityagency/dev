<?php

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ParameterBag;
use SoundCloudApp\Providers\UserProvider;

$app->get('/connexion', function () use ($app){
    //require '../src/model.php';
    //$articles = getArticles();
    //var_dump($request) ;
    //var_dump($app);
    ob_start();             // start buffering HTML output
    // require fonctionne pour les include PHP mais pas pour twig.$_COOKIE
    // pour twig faire render ...

    // redirection
    return $app->redirect($app['url_generator']->generate('inscription'));
})->bind('connexion');

?>