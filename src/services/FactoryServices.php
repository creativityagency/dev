<?php
use Symfony\Component\HttpFoundation\Request;
Class FactoryServices {
	/**
	 * Enregistre dans une variable session les infos d'un visiteur
	 * id, username, lastname, email, role 
	 */
	public function connecter($id, $username, $lastname, $email, $role) {
		$_SESSION['id'] = $id;
		$_SESSION['username'] = $username;
		$_SESSION['lastname'] = $lastname;
		$_SESSION['email'] = $email;
		$_SESSION['role'] = $role;
	}

	public function testservices() {
		return "chaine de testtesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttest";
	}

	/**
	 * Teste si un quelconque visiteur est connecté
	 * @return vrai ou faux si la session ID existe
	 */
	public function estConnecte() {
		return isset ( $_SESSION['id'] );
	}

	/**
	 * Détruit la session active
	 */
	public function deconnecter() {

		session_start();
		session_destroy();
	}

	/**
	 * Ajoute le libellé d'une erreur au tableau des erreurs
	 *
	 * @param $msg :
	 *        	le libellé de l'erreur
	 */
	 function ajouterErreur($msg) {
		if (! isset ( $_REQUEST ['erreurs'] )) {
			$_REQUEST ['erreurs'] = array ();
		}
		$_REQUEST ['erreurs'] [] = $msg;
	}


}

?>
