﻿<?php
/**
 * Classe d'accès aux données.

 * Utilise les services de la classe PDO
 * pour l'application
 * Les attributs sont tous statiques,
 * les 4 premiers pour la connexion
 * $myDatabase de type PDO
 * $myDatabaseClass qui contiendra l'unique instance de la classe
 */

class Database {

	private static $serveur = 'mysql:host=localhost';
	private static $bdd = 'dbname=espace-membre';
	private static $user = 'root';
	private static $mdp = 'root';
	private static $myDatabase;
	private static $myDatabaseClass = null;

	/**
	 * Constructeur privé, crée l'instance de Database qui sera sollicitée
	 * pour toutes les méthodes de la classe
	 */
	private function __construct() {
		Database::$myDatabase = new PDO ( Database::$serveur . ';' . Database::$bdd, Database::$user, Database::$mdp );
		Database::$myDatabase->query ("SET CHARACTER SET utf8");
	}
	
	public function _destruct() {
		Database::$myDatabase = null;
	}

	/**
	 * Fonction statique qui crée l'unique instance de la classe
	 *
	 * Appel : $instance = Database::getDatabase();
	 *
	 * @return l'unique objet de la classe Database
	 */
	public static function getDatabase() {
		if (Database::$myDatabaseClass == null) {
			Database::$myDatabaseClass = new Database();
		}
		return Database::$myDatabaseClass;
	}


	public function testtest()
	{
		return "chainechainechainechainechainechaine";
	}

	public function recupLog()
	{
		$sql = Database::$myDatabase->prepare ( "SELECT * FROM connection_log");
		$sql->execute ();
		$req = $sql->fetch();
		return $req;
	}

	/** 
	 * Methode qui va recuperer les infos de l'utilisateur 
	 * id, username, lastname, password, email, role 
	 */

	public function getInfosUser($email, $password) {
		$sql = Database::$myDatabase->prepare ( "SELECT id, username, lastname, password, email, role
											 FROM users
											 WHERE email = :email AND password = :password");
		$sql->execute (array(":email" => $email , ":password" => $password));
		$req = $sql->fetch();
		return $req;
	}




  





	public function getInfosGestionnaire($login, $mdp)
        {
		$req = "select gestionnaire.id as id, gestionnaire.login as login, gestionnaire.nom as nom,gestionnaire.prenom as prenom, gestionnaire.mdp as mdp from gestionnaire
		where gestionnaire.login='$login' and gestionnaire.mdp='$mdp'";
		$rs = PdoClass::$monPdo->query ( $req );
		$ligne = $rs->fetch();
		return $ligne;
	}
	/**
	 * Retourne sous forme d'un tableau associatif toutes les lignes de frais au forfait
	 * concernées par les deux arguments
	 *
	 * @param
	 *        	$idVisiteur
	 * @param $mois sous
	 *        	la forme aaaamm
	 * @return l'id, le libelle et la quantité sous la forme d'un tableau associatif
	 *
	 */
	public function getLesFraisForfait($idVisiteur, $mois) {
		$req = "select fraisforfait.id as idfrais, fraisforfait.libelle as libelle,
		lignefraisforfait.quantite as quantite from lignefraisforfait inner join fraisforfait
		on fraisforfait.id = lignefraisforfait.idfraisforfait
		where lignefraisforfait.idvisiteur ='$idVisiteur' and lignefraisforfait.mois='$mois'
		order by lignefraisforfait.idfraisforfait";
		$res = PdoGsb::$monPdo->query ( $req );
		$lesLignes = $res->fetchAll ();
		return $lesLignes;
	}
	/**
	 * Retourne tous les id de la table FraisForfait
	 *
	 * @return un tableau associatif
	 *
	 */
	public function getLesIdFrais() {
		$req = "select fraisforfait.id as idfrais from fraisforfait order by fraisforfait.id";
		$res = PdoGsb::$monPdo->query ( $req );
		$lesLignes = $res->fetchAll ();
		return $lesLignes;
	}
	/**
	 * Met à jour la table ligneFraisForfait
	 *
	 * Met à jour la table ligneFraisForfait pour un visiteur et
	 * un mois donné en enregistrant les nouveaux montants
	 *
	 * @param
	 *        	$idVisiteur
	 * @param $mois sous
	 *        	la forme aaaamm
	 * @param $lesFrais tableau
	 *        	associatif de clé idFrais et de valeur la quantité pour ce frais
	 * @return un tableau associatif
	 *
	 */
	public function majFraisForfait($idVisiteur, $mois, $lesFrais) {
		$lesCles = array_keys ( $lesFrais );
		foreach ( $lesCles as $unIdFrais ) {
			$qte = $lesFrais [$unIdFrais];
			$req = "update lignefraisforfait set lignefraisforfait.quantite = $qte
			where lignefraisforfait.idvisiteur = '$idVisiteur' and lignefraisforfait.mois = '$mois'
			and lignefraisforfait.idfraisforfait = '$unIdFrais'";
			PdoGsb::$monPdo->exec ( $req );
		}
	}

	/**
	 * Teste si un visiteur possède une fiche de frais pour le mois passé en argument
	 *
	 * @param
	 *        	$idVisiteur
	 * @param $mois sous
	 *        	la forme aaaamm
	 * @return vrai ou faux
	 *
	 */
	public function estPremierFraisMois($idVisiteur, $mois) {
		$ok = false;
		$req = "select count(*) as nblignesfrais from fichefrais
		where fichefrais.mois = '$mois' and fichefrais.idvisiteur = '$idVisiteur'";
		$res = PdoGsb::$monPdo->query ( $req );
		$laLigne = $res->fetch ();
		if ($laLigne ['nblignesfrais'] == 0) {
			$ok = true;
		}
		return $ok;
	}
	/**
	 * Retourne le dernier mois en cours d'un visiteur
	 *
	 * @param
	 *        	$idVisiteur
	 * @return le mois sous la forme aaaamm
	 *
	 */
	public function dernierMoisSaisi($idVisiteur) {
		$req = "select max(mois) as dernierMois from fichefrais where fichefrais.idvisiteur = '$idVisiteur'";
		$res = PdoGsb::$monPdo->query ( $req );
		$laLigne = $res->fetch ();
		$dernierMois = $laLigne ['dernierMois'];
		return $dernierMois;
	}

	/**
	 * Crée une nouvelle fiche de frais et les lignes de frais au forfait pour un visiteur et un mois donnés
	 *
	 * récupère le dernier mois en cours de traitement, met à 'CL' son champs idEtat, crée une nouvelle fiche de frais
	 * avec un idEtat à 'CR' et crée les lignes de frais forfait de quantités nulles
	 *
	 * @param
	 *        	$idVisiteur
	 * @param $mois sous
	 *        	la forme aaaamm
	 *
	 */
	public function creeNouvellesLignesFrais($idVisiteur, $mois) {
		$dernierMois = $this->dernierMoisSaisi ( $idVisiteur );
		$laDerniereFiche = $this->getLesInfosFicheFrais ( $idVisiteur, $dernierMois );
		if ($laDerniereFiche ['idEtat'] == 'CR') {
			$this->majEtatFicheFrais ( $idVisiteur, $dernierMois, 'CL' );
		}
		$req = "insert into fichefrais(idvisiteur,mois,nbJustificatifs,montantValide,dateModif,idEtat)
		values('$idVisiteur','$mois',0,0,now(),'CR')";
		PdoGsb::$monPdo->exec ( $req );
		$lesIdFrais = $this->getLesIdFrais ();
		foreach ( $lesIdFrais as $uneLigneIdFrais ) {
			$unIdFrais = $uneLigneIdFrais ['idfrais'];
			$req = "insert into lignefraisforfait(idvisiteur,mois,idFraisForfait,quantite)
			values('$idVisiteur','$mois','$unIdFrais',0)";
			PdoGsb::$monPdo->exec ( $req );
		}
	}

	/**
	 * Retourne les mois pour lesquel un visiteur a une fiche de frais
	 *
	 * @param
	 *        	$idVisiteur
	 * @return un tableau associatif de clé un mois -aaaamm- et de valeurs l'année et le mois correspondant
	 *
	 */
	public function getLesMoisDisponibles($idVisiteur) {
		$req = "select fichefrais.mois as mois from  fichefrais where fichefrais.idvisiteur ='$idVisiteur'
		order by fichefrais.mois desc ";
		$res = PdoGsb::$monPdo->query ( $req );
		$lesMois = array ();
		$laLigne = $res->fetch ();
		while ( $laLigne != null ) {
			$mois = $laLigne ['mois'];
			$numAnnee = substr ( $mois, 0, 4 );
			$numMois = substr ( $mois, 4, 2 );
			$lesMois ["$mois"] = array (
					"mois" => "$mois",
					"numAnnee" => "$numAnnee",
					"numMois" => "$numMois"
			);
			$laLigne = $res->fetch ();
		}
		return $lesMois;
	}
	/**
	 * Retourne les informations d'une fiche de frais d'un visiteur pour un mois donné
	 *
	 * @param
	 *        	$idVisiteur
	 * @param $mois sous
	 *        	la forme aaaamm
	 * @return un tableau avec des champs de jointure entre une fiche de frais et la ligne d'état
	 *
	 */
	public function getLesInfosFicheFrais($idVisiteur, $mois) {
		$req = "select fichefrais.idEtat as idEtat, fichefrais.dateModif as dateModif, fichefrais.nbJustificatifs as nbJustificatifs,
			fichefrais.montantValide as montantValide, etat.libelle as libEtat from  fichefrais inner join etat on fichefrais.idEtat = etat.id
			where fichefrais.idvisiteur ='$idVisiteur' and fichefrais.mois = '$mois'";
		$res = PdoGsb::$monPdo->query ( $req );
		$laLigne = $res->fetch ();
		return $laLigne;
	}
	/**
	 * Modifie l'état et la date de modification d'une fiche de frais
	 *
	 * Modifie le champ idEtat et met la date de modif à aujourd'hui
	 *
	 * @param
	 *        	$idVisiteur
	 * @param $mois sous
	 *        	la forme aaaamm
	 */
	public function majEtatFicheFrais($idVisiteur, $mois, $etat) {
		$req = "update ficheFrais set idEtat = '$etat', dateModif = now()
		where fichefrais.idvisiteur ='$idVisiteur' and fichefrais.mois = '$mois'";
		PdoGsb::$monPdo->exec ( $req );
	}
	/*
	 * Param @login
	 * Param @mdp
	 *
	 * Infos de connexion du gestionnaire
	 */
	public function getAjouterVisiteur($id,$mdp, $nom, $prenom, $login, $adresse, $cp, $ville, $date) {
		if (isset ( $_POST ['submit_visiteur'] )) {

			$sql = "insert into visiteur(id, nom, prenom, adresse, cp, ville, dateEmbauche, login, mdp)
		values(:id, :nom, :prenom, :adresse, :cp, :ville, :dateEmbauche, :login, :mdp);";




			$req = PdoGsb::$monPdo->prepare ( $sql );
			$req->bindParam ( ':id', $id );
			$req->bindParam ( ':nom', $nom );
			$req->bindParam ( ':prenom', $prenom );
			$req->bindParam ( ':adresse', $adresse );
			$req->bindParam ( ':cp', $cp );
			$req->bindParam ( ':ville', $ville );
			$req->bindParam ( ':dateEmbauche', $date );
			$req->bindParam ( ':login', $login );
			$req->bindParam ( ':mdp', $mdp );
			$req->execute ();
		} /*
		   * $req = "insert into visiteur values('$id', '$nom', '$prenom', '$login', '$mdp' ,'$adresse', '$cp', '$ville', '$date')";
		   * $rs = PdoGsb::$monPdo->query($req);
		   */
	}
	public function getAfficherVisiteurPdf() {
		$sql = PdoGsb::$monPdo->prepare ( "select * from visiteur order by visiteur.dateEmbauche asc" );
		$sql->execute ();
		$req = $sql->fetchAll ();
		return $req;
	}
	public function loginReturn($login) {
		$sql = "SELECT login FROM visiteur WHERE login = '$login'";
		$rs = PdoGsb::$monPdo->query ( $sql );
		$reponseSQL = $rs->fetch ();
		return $reponseSQL;
	}
        	public function getLesVisiteurs()
	{

		$sql = PdoGsb::$monPdo->prepare("select id, nom, prenom from visiteur") ;
		$sql->execute ();
		$tab = $sql->fetchAll();
		return $tab;
	}



	// Modification



	public function getLeVisiteur($id)
	{
			$sql = PdoGsb::$monPdo->prepare("SELECT * FROM visiteur WHERE id = :id");
			$sql->bindParam ( ':id', $id);
			$sql->execute();
			$lignes = $sql->fetch(); //recupere une ligne visiteur
			return $lignes ;
	}


	public function ModifierVisiteur($nom, $prenom, $login,$adresse, $cp, $ville, $id)
	{
			$sql2 = PdoGsb::$monPdo->prepare("UPDATE visiteur
					SET nom = ?,	prenom = ?,	login = ?,	adresse = ?,	cp = ?,	ville = ?
					WHERE id = ?");

					$sql2->bindParam( 1, $nom);
					$sql2->bindParam( 2, $prenom );
					$sql2->bindParam( 3, $login );
					$sql2->bindParam( 4, $adresse );
					$sql2->bindParam( 5, $cp );
					$sql2->bindParam( 6, $ville );
					$sql2->bindParam( 7, $id );
					$sql2->execute();

	}


	public function ModifierMotDePasse( $mdp, $id)
	{
			$sql2 = PdoGsb::$monPdo->prepare("UPDATE visiteur
					SET mdp = ? WHERE id = ?");
					var_dump($id);
					var_dump($mdp);
					$sql2->bindParam( 1, $mdp);
					$sql2->bindParam( 2, $id );
					var_dump($sql2);
					$sql2->execute();

	}
}

?>
