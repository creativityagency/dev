<?php

// Importer nos dépendances externes
use Silex\Application;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\HttpFragmentServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\VarDumperServiceProvider;
use Silex\Provider\FormServiceProvider;
use Silex\Provider\ValidatorServiceProvider;
use Silex\Provider\LocaleServiceProvider;
use Silex\Provider\SessionServiceProvider;
use Silex\Provider\TranslationServiceProvider;
use Silex\Provider\CsrfServiceProvider;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

// Initialiser l'application Silex
$app = new Application();
 
// Register les services que l'on veut utiliser
$app->register(new SessionServiceProvider());
$app->register(new TwigServiceProvider());
$app->register(new HttpFragmentServiceProvider());
$app->register(new ServiceControllerServiceProvider());
$app->register(new VarDumperServiceProvider());
$app->register(new FormServiceProvider());
$app->register(new CsrfServiceProvider());
$app->register(new ValidatorServiceProvider());
$app->register(new LocaleServiceProvider());
$app->register(new TranslationServiceProvider());

// Register error handler
$app->error(function (\Exception $e, Request $request, $code) {
    switch ($code) {
        case 403:
            $message = 'Access denied.';
            break;
        case 404:
            $message = 'The requested resource could not be found.';
            break;
        case 500:
            $message = 'Internal Server Error.';
            break;
        default:
            $message = "Something went wrong.";
    }
    echo '<p>' . $message  . '</p>';
});


// Active les logs
$app->register(new Silex\Provider\MonologServiceProvider(), array(
    'monolog.logfile' => __DIR__.'/../var/logs/development.log',
));

return $app;
